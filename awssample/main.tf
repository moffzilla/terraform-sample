provider "aws" {
    version = "2.69.0"
    region="us-west-2"
}

resource "aws_instance" "machine1" {
    ami           = "ami-22741f5a"
    instance_type = var.instance_type
    availability_zone = "us-west-2a"
    tags = {
      "type" = var.myTag
    }
}

#resource "aws_ebs_volume" "example" {
#  availability_zone = "us-west-2a"
#  size              = 4
#}

#resource "aws_volume_attachment" "ebs_att" {
#  device_name = "/dev/sdh"
#  volume_id   = aws_ebs_volume.example.id
#  instance_id = aws_instance.machine1.id
#}


